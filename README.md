# IoT Platform Project (WEB) #

> Qualcomm Institute 동계 현장학습 인턴십 프로젝트


### 요구사항 정의
-----------------
 - 요구사항: https://trello.com/b/H4h41yy8/%EC%9A%94%EA%B5%AC-%EC%82%AC%ED%95%AD
 - 기능 (Trello): https://trello.com/b/4OEbonfx/function
 - 기능 (Text): https://docs.google.com/document/d/1zQsi_opPK-Dt2oowp8CeBQYJ6_cYFcPh2jb3af2xkm4/edit
 - 데이터베이스: https://trello.com/b/hi0eKYsd/database
 

### 설계
-----------------
 - 시스템 설계: https://www.lucidchart.com/documents/viewVisio/d7111060-e1a8-488d-9424-a52f3f56f9f5
 - 흐름도: https://drive.google.com/drive/folders/1iLiAPrX4twRlZDoDkcp3Yhi5uKnuCBSn
 - 데이터베이스 ERD: https://drive.google.com/drive/folders/1G8V9XX9YY7L4qncRZZSq_fbHIc5PjWo8
 - Restful API 명세서: https://docs.google.com/document/d/1XRqVN9bfgP0eXDqtqHFPX3g57-_Dghw42GGNJr1QfaY/edit?usp=sharing


### 구현
-----------------
> PHP, Slim framework, JWT(Json Web Token)

 - Frontend: https://bitbucket.org/16byte/teamb-iot/src/master/public/
 - Backend: https://bitbucket.org/16byte/teamb-iot/src/master/apps/iot/


### 주차별 발표 자료
-----------------
 - 1주차: https://docs.google.com/presentation/d/1oqsBIINXGFWxJz5M1wtpwAXQXQ-OktDlyaVIFubMfas/edit?usp=sharing
 - 2주차: https://docs.google.com/presentation/d/1xxOQlKMAcu4D1ZcA861a893QMlyv9euxM0Ugi2KtZC0/edit?usp=sharing
 - 3주차: https://docs.google.com/presentation/d/1c7ulkvwbrzEHiOP2kBBetZ5zOKeYGEKLshQsE0R6lls/edit?usp=sharing
 - 4주차: https://docs.google.com/presentation/d/1Ocv5BFY3Wn_AWow0OaZI6S6QGqX8ppuicvlx62pcMok/edit?usp=sharing
 - 5주차: https://docs.google.com/presentation/d/1p3aaHX6-thta5gTPIyH104JW23VqjBbhsoSrpOVcuHI/edit?usp=sharing
 - Final: https://drive.google.com/file/d/1b6rdLwlYMu3-jNMIQejgfUvzUPkhDgT7/view?usp=sharing

## License
This project is released under the MIT public license.
